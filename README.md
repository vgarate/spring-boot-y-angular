## IDE VScode

Proyecto realizado en Visual Studio Code, descargando los plugin correspondientes;`Spring Boot Tools`, `Spring Boots Extension Pack` entre otros.   Revisa su instalación y Hola mundo con [VScode maven](https://www.youtube.com/watch?v=epDPEphSYSI&t=193s) subido por SpringDeveloper en Septiembre del 2018.

## Spring Boot 

BackEnd realizado con Sprin Boot, realizar pruebas en base de datos `tutorial` tabla `usuarios`, sus respectivas variables se encuentran en la carpeta model.

## Proyecto

Se levanta en el puerto 7878 y su FrontEnd se encuentra [aquí](https://gitlab.com/Radamhantiz/frontendangular.git) realizado con Angula 7.   Correr el proyecto directamente de la carpeta `springbootangulardemo` en el archivo `DemoAplication.java`.

## El mundo arder

Sé que muchos diría "para qué si vscode si Spring tiene su IDE", bueno... encontré tan comodo vscode que si es necesario programar y cargar mis proyectos electronicos en assembler... lo haré...
`Moraleja`: Muchos te diran qué es mejor, pero nadie mejor que tu sabe que son sólo consejos para mejorar tu lugar de confort.
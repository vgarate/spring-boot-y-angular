package com.crud.springbootangulardemo.controllers;

import java.io.IOException;
import java.util.List;

import com.crud.springbootangulardemo.model.User;
import com.crud.springbootangulardemo.service.UserService;
import com.crud.springbootangulardemo.utilidad.RestResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    protected UserService userService;

    protected ObjectMapper mapper;

    /**
     * Guarda y actualiza los datos de usuarios
     * @param userJson
     * @return la validación 
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
    public RestResponse saveOrUpdate(@RequestBody String userJson) throws JsonParseException, JsonMappingException, IOException{

        this.mapper = new ObjectMapper();

        User user = this.mapper.readValue(userJson, User.class);

        if (!this.validate(user)) {
            return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), "Los campos obligatorios no estan completados.");
        }
        this.userService.save(user);

        return new RestResponse(HttpStatus.OK.value(), "Operación exitosa");
    }

    /**
     * Busca los usuarios TODOS
     * @return una lista con los usuarios
     */
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public List<User> getUsers(){
        return this.userService.findAll();
    }

    /**
     * Eliminar un usuario
     * @param userJson
     * @throws Exception
     */
    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public void deleteUser(@RequestBody String userJson) throws Exception {
        this.mapper = new ObjectMapper();

        User user = this.mapper.readValue(userJson, User.class);

        if (user.getId() == null) {
            throw new Exception("El id está nulo.");
        }

        this.userService.deleteUser(user.getId());
    }

    private boolean validate(User user) {

        boolean isValid = true;

        if (StringUtils.trimToNull(user.getPrimerNombre()) == null) {
            isValid = false;
        }

        if (StringUtils.trimToNull(user.getPrimerApellido()) == null) {
            isValid = false;                
        }

        if (StringUtils.trimToNull(user.getEmail()) == null) {
            isValid = false;                
        }

        if (StringUtils.trimToNull(user.getDireccion()) == null) {
            isValid = false;
        }
        return isValid;
    }
}
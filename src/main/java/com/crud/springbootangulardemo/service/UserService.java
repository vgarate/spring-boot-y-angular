package com.crud.springbootangulardemo.service;

import java.util.List;

import com.crud.springbootangulardemo.model.User;

public interface UserService {

	/**
	 * Guarda usuario
	 * @param user
	 * @return el usuario que se guardó
	 */
	User save(User user);

	/**
	 * Recupera la lista de los usuarios
	 * @return una lista con usuarios
	 */
	List<User> findAll();

	/**
	 * Elimina el usuario con el id recibido
	 * @param id
	 */
	void deleteUser(Long id);
}